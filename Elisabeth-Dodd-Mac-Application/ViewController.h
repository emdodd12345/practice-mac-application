//
//  ViewController.h
//  ElisabethDoddApp
//
//  Created by Elisabeth Dodd on 5/4/16.
//  Copyright © 2016 Elisabeth Dodd. All rights reserved.
//

#import "RVCustomView.h"
#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

@property (weak) IBOutlet RVCustomView *myGradientView;
@property (weak) IBOutlet NSBox *rightBorder;
@property (weak) IBOutlet NSBox *leftBorder;
@property (weak) IBOutlet NSStepperCell *stepper;
@property (weak) IBOutlet NSLayoutConstraint *topBorderConstraint;
@property (weak) IBOutlet NSLayoutConstraint *topBorderLeading;
@property (weak) IBOutlet NSLayoutConstraint *bottomBorderConstraint;
@property (weak) IBOutlet NSTextField *textField;
@property (weak) IBOutlet NSBox *topBorder;
@property (weak) IBOutlet NSBox *bottomBorder;
@property (weak) IBOutlet NSPopUpButton *borderColorMenu;
@property (weak) IBOutlet NSPopUpButton *fillColor1;
@property (weak) IBOutlet NSPopUpButton *fillColor2;
@property (weak) IBOutlet NSButton *btnToggleGradient;
@property (weak) IBOutlet NSButton *btnTopBorder;
@property (weak) IBOutlet NSButton *btnRightBorder;
@property (weak) IBOutlet NSButton *btnBottomBorder;
@property (weak) IBOutlet NSButton *btnLeftBorder;
@property (weak) IBOutlet NSButton *btnBorder;


- (IBAction)controlDidChange:(id)sender;
- (IBAction)toggleBorder:(id)sender;
- (IBAction)toggleRightBorder:(id)sender;
- (IBAction)toggleLeftBorder:(id)sender;
- (IBAction)toggleTopBorder:(id)sender;
- (IBAction)toggleBottomBorder:(id)sender;
- (IBAction)updateBorderColor:(id)sender;
- (IBAction)toggleGradient:(id)sender;
- (IBAction)updateFillColor1:(id)sender;
- (IBAction)updateFillColor2:(id)sender;




@end

