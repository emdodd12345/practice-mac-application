//
//  ViewController.m
//  ElisabethDoddApp
//
//  Created by Elisabeth Dodd on 5/4/16.
//  Copyright © 2016 Elisabeth Dodd. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.textField setIntValue:10];
    [self.myGradientView setBorderHidden:NO];
    [self.myGradientView setTopBorderHidden:NO];
    [self.myGradientView setRightBorderHidden:NO];
    [self.myGradientView setLeftBorderHidden:NO];
    [self.myGradientView setBottomBorderHidden:NO];
    [self.myGradientView setBorderThickness:10];
    [self.myGradientView setStartingColor:
     [NSColor redColor]];
    [self.myGradientView setEndingColor:
     [NSColor blueColor]];
    [self.myGradientView setAngle:270];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
    
}

//This function handles changes to Border Thickness. 
- (IBAction)controlDidChange:(id)sender {
    if([sender integerValue] == 0){
        [self.leftBorder setHidden:YES];
        [self.rightBorder setHidden:YES];
        [self.topBorder setHidden:YES];
    }
    NSInteger difference = [self.myGradientView getBorderThickness] - [sender integerValue];
    if([self.myGradientView getBorderThickness]<[sender integerValue]){
        if(self.rightBorder.hidden == YES && [self.myGradientView getRightBorderHidden] == NO){
            [self.rightBorder setHidden:NO];
        }
        if(self.leftBorder.hidden == YES && [self.myGradientView getLeftBorderHidden] == NO){
            [self.leftBorder setHidden:NO];
        }
        if(self.topBorder.hidden == YES && [self.myGradientView getTopBorderHidden] == NO){
            [self.topBorder setHidden:NO];
        }
    }
    [self updateBorder:difference];
    [self.myGradientView setBorderThickness:[sender integerValue]];
    [self updateControls];
    
}

- (void) updateControls
{
    [self.stepper setIntegerValue:[self.myGradientView getBorderThickness]];
    [self.textField setIntegerValue:[self.myGradientView getBorderThickness]];
}

- (void) updateBorder:(NSInteger)difference {
    self.topBorderConstraint.constant+=difference;
    self.topBorderLeading.constant+=difference;
    self.bottomBorderConstraint.constant+=difference;
}

- (IBAction)toggleBorder:(id)sender {
    //if border is showing
    if(![self.myGradientView getBorderHidden]){
        if(![self.myGradientView getTopBorderHidden]){
            [self.btnTopBorder performClick:self];
        }
        if(![self.myGradientView getLeftBorderHidden]){
            [self.btnLeftBorder performClick:self];
        }
        if(![self.myGradientView getRightBorderHidden]){
            [self.btnRightBorder performClick:self];
        }
        if(![self.myGradientView getBottomBorderHidden]){
            [self.btnBottomBorder performClick:self];
        }
        [self.myGradientView setBorderHidden:YES];
        [sender setTitle:@"Show Border"];
    }else{
        if([self.myGradientView getTopBorderHidden]){
            [self.btnTopBorder performClick:self];
        }
        if([self.myGradientView getLeftBorderHidden]){
            [self.btnLeftBorder performClick:self];
        }
        if([self.myGradientView getRightBorderHidden]){
            [self.btnRightBorder performClick:self];
        }
        if([self.myGradientView getBottomBorderHidden]){
            [self.btnBottomBorder performClick:self];
        }
        [self.myGradientView setBorderHidden:NO];
        [sender setTitle:@"Hide Border"];
    }
}

//After an individual border is hidden, this function will check and if all the other borders are hidden, it will set borderHidden to YES and change "Hide Border" to "Show Border"
- (void) checkIfBorderHidden{
    if([self.myGradientView getTopBorderHidden] && [self.myGradientView getLeftBorderHidden] && [self.myGradientView getRightBorderHidden] && [self.myGradientView getBottomBorderHidden]){
        [self.myGradientView setBorderHidden:YES];
        [self.btnBorder setTitle:@"Show Border"];
    }
}

//Similar to as checkIfBorderHidden except checks if all are showing.
- (void) checkIfBorderShowing{
    if(![self.myGradientView getTopBorderHidden] && ![self.myGradientView getLeftBorderHidden] && ![self.myGradientView getRightBorderHidden] && ![self.myGradientView getBottomBorderHidden]){
        [self.myGradientView setBorderHidden:NO];
        [self.btnBorder setTitle:@"Hide Border"];
    }
}


- (IBAction)toggleRightBorder:(id)sender {
    if([self.myGradientView getRightBorderHidden]){
        [self.rightBorder setHidden:NO];
        [self.myGradientView setRightBorderHidden:NO];
        [sender setTitle:@"Hide Right Border"];
        [self checkIfBorderShowing];
    }else{
        [self.rightBorder setHidden:YES];
        [self.myGradientView setRightBorderHidden:YES];
        [sender setTitle:@"Show Right Border"];
        [self checkIfBorderHidden];
    }
    
}
- (IBAction)toggleLeftBorder:(id)sender {
    if([self.myGradientView getLeftBorderHidden]){
        [self.leftBorder setHidden:NO];
        [self.myGradientView setLeftBorderHidden:NO];
        [sender setTitle:@"Hide Left Border"];
        [self checkIfBorderShowing];
    }else{
        [self.leftBorder setHidden:YES];
        [self.myGradientView setLeftBorderHidden:YES];
        [sender setTitle:@"Show Left Border"];
        [self checkIfBorderHidden];
    }

}

- (IBAction)toggleTopBorder:(id)sender {
    if([self.myGradientView getTopBorderHidden]){
        [self.topBorder setHidden:NO];
        [self.myGradientView setTopBorderHidden:NO];
        [sender setTitle:@"Hide Top Border"];
        [self checkIfBorderShowing];
    }else{
        [self.topBorder setHidden:YES];
        [self.myGradientView setTopBorderHidden:YES];
        [sender setTitle:@"Show Top Border"];
        [self checkIfBorderHidden];
    }

}

- (IBAction)toggleBottomBorder:(id)sender {
    if([self.myGradientView getBottomBorderHidden]){
        [self.bottomBorder setHidden:NO];
        [self.myGradientView setBottomBorderHidden:NO];
        [sender setTitle:@"Hide Bottom Border"];
        [self checkIfBorderShowing];
    }else{
        [self.bottomBorder setHidden:YES];
        [self.myGradientView setBottomBorderHidden:YES];
        [sender setTitle:@"Show Bottom Border"];
        [self checkIfBorderHidden];
    }

}

- (IBAction)updateBorderColor:(id)sender {
    NSColor* borderColor = [self getColorFromIndex:[sender indexOfSelectedItem]];
    [self changeBorderColor:borderColor];
}

- (IBAction)toggleGradient:(id)sender {
    if(self.btnToggleGradient.state == 0){
        //hide fill color 2 menu
        [self.fillColor2 setHidden:YES];
        [self.myGradientView setEndingColor:
         [self getColorFromIndex:[self.fillColor1 indexOfSelectedItem]]];
        [self.myGradientView changeGradient];
        //set bottom color to same color as top
    }else{
        //show fill color 2 menu
        [self.fillColor2 setHidden:NO];
        //set bottom color to whatever it was already
        [self.myGradientView setEndingColor:
         [self getColorFromIndex:[self.fillColor2 indexOfSelectedItem]]];
        [self.myGradientView changeGradient];
    }
}

- (IBAction)updateFillColor1:(id)sender {
    [self.myGradientView setStartingColor:
     [self getColorFromIndex:[sender indexOfSelectedItem]]];
    if(self.fillColor2.hidden){
        [self.myGradientView setEndingColor:
         [self getColorFromIndex:[sender indexOfSelectedItem]]];
    }
    [self.myGradientView changeGradient];
}

- (IBAction)updateFillColor2:(id)sender {
    [self.myGradientView setEndingColor:
     [self getColorFromIndex:[sender indexOfSelectedItem]]];
    [self.myGradientView changeGradient];
}

- (void)changeBorderColor:(NSColor *)color {
    self.topBorder.fillColor = color;
    self.topBorder.borderColor = color;
    self.bottomBorder.fillColor = color;
    self.bottomBorder.borderColor = color;
    self.rightBorder.fillColor = color;
    self.rightBorder.borderColor = color;
    self.leftBorder.fillColor = color;
    self.leftBorder.borderColor = color;
}

- (NSColor *)getColorFromIndex:(NSInteger)index{
    NSColor* color;
    switch(index){
        case 0:
            color = [NSColor blackColor];
            break;
        case 1:
            color = [NSColor redColor];
            break;
        case 2:
            color = [NSColor orangeColor];
            break;
        case 3:
            color = [NSColor yellowColor];
            break;
        case 4:
            color = [NSColor greenColor];
            break;
        case 5:
            color = [NSColor blueColor];
            break;
        case 6:
            color = [NSColor purpleColor];
            break;
        case 7:
            color = [NSColor whiteColor];
            break;
        default:
            color = [NSColor blackColor];
            break;
            
    }
    return color;
}

@end
