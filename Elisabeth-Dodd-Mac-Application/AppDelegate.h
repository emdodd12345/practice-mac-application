//
//  AppDelegate.h
//  Elisabeth-Dodd-Mac-Application
//
//  Created by Elisabeth Dodd on 5/4/16.
//  Copyright © 2016 Elisabeth Dodd. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

