//
//  RVCustomView.h
//  ElisabethDoddApp
//
//  Created by Elisabeth Dodd on 5/4/16.
//  Copyright © 2016 Elisabeth Dodd. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@import AppKit;
@import QuartzCore;

IB_DESIGNABLE

@interface RVCustomView : NSView
{
    NSGradient* aGradient;
    NSColor *startingColor;
    NSColor *endingColor;
    int angle;
}
@property(readwrite, assign) IBOutlet RVCustomView *gradientView;
@property (strong, nonatomic) NSGradient* aGradient;
@property(nonatomic, retain) NSColor *startingColor;
@property(nonatomic, retain) NSColor *endingColor;
@property(assign) int angle;
@property (getter = getBorderThickness) NSInteger borderThickness;
@property (getter = getBorderHidden) BOOL borderHidden;
@property (getter = getRightBorderHidden) BOOL rightBorderHidden;
@property (getter = getLeftBorderHidden) BOOL leftBorderHidden;
@property (getter = getTopBorderHidden) BOOL topBorderHidden;
@property (getter = getBottomBorderHidden) BOOL bottomBorderHidden;

- (void)changeGradient;
@end
