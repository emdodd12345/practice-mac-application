//
//  RVCustomView.m
//  ElisabethDoddApp
//
//  Created by Elisabeth Dodd on 5/4/16.
//  Copyright © 2016 Elisabeth Dodd. All rights reserved.
//

#import "RVCustomView.h"

@implementation RVCustomView

@synthesize startingColor;
@synthesize endingColor;
@synthesize angle;
@synthesize aGradient;
@synthesize gradientView;

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
        [self setStartingColor:[NSColor colorWithCalibratedWhite:1.0 alpha:1.0]];
        [self setEndingColor:nil];
        [self setAngle:270];
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
    if (endingColor == nil || [startingColor isEqual:endingColor]) {
        [startingColor set];
        NSRectFill(rect);
    }
    else {
        aGradient = [[NSGradient alloc]
                                 initWithStartingColor:startingColor
                                 endingColor:endingColor];
        [aGradient drawInRect:[self bounds] angle:angle];
    }
}

- (void)changeGradient {
    [self setNeedsDisplay:YES];
}

@end
